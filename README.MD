- This is a Laravel 5.1 application designed to take a string under 200 characters and a number (int/float with up to 2 decimal places) and convert the number into a meaningful english string representing the dollar value of the number.

CHEQUE WRITER APPLICATION INSTALL

1.	Download repo, public accessible to ssh on: git clone bitbucket.org:Fredylg/float-to-string.git   or https on: git clone https://bitbucket.org/Fredylg/float-to-string.git
2.	Set permissions over storage folder.
3.	Run: composer install
4.	Set up your .env file according to your environment settings.
[NOTE] For live environments do not forget to hide error logs.
5.	Regenerate you local key: php artisan key:generate
[NOTE] At this point the site should be rendering content the next steps will guide you into installing the frontend task manager and js minifier.
6.	Run the node package manager installer: npm install
7.	Run the frontend package manager installer: bower install
8.	Run the tasks manager Gulp: gulp --production
[NOTE] make sure permissions over folder are properly set and the user running    the task belongs to the apache2 / nginx group.











- Written by Fredy Lievano fredylg@hotmai.com