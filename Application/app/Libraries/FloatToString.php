<?php
Namespace App\Libraries;

/**
 * Class FloatToString
 * @package App\Libraries
 */
class FloatToString{
    /**
     * protected attributes...
     * @var string
     */
    protected $float , $cents , $dollars, $lan = 'en_AU';
    /**
     * @var array
     */
    protected $dec = [ 'hundred' , 'thousand' , 'million' , 'billion'];


    /**
     * Split cents and dollars
     * @param mixed $float
     */
    public function setFloat($float)
    {
        $this->float =  number_format( $float , 2 );
        $values_array = explode( '.' , $this->float );
        $this->cents = $values_array [ 1 ];
        $this->dollars = str_replace(',' ,'' ,  $values_array [ 0 ] );
    }

    /**
     * @param $float
     * @return string
     * Set data and return
     */
    public function getString( $float )
    {
        $this->setFloat( $float );
        return $this->numToString();
    }

    /**
     * @return string
     * Format number to string
     */
    public function numToString( )
    {
        $f = new \NumberFormatter( $this->lan, \NumberFormatter::SPELLOUT );
        $dollars =  $f->format( $this->dollars  );
        $cents  =  $f->format( $this->cents );

        if( $this->dollars > 1 ){
            $dollar_string = ' dollars and ';
        }else if ( $this->dollars < 1 ){
            $dollar_string = '';
            $dollars = '';
        }else{
            $dollar_string = ' dollar and ';
        }

        if( $cents == 'one' ){
            $cents_string = ' cent';
        }else{
            $cents_string = ' cents';

        }

        $string = $dollars.$dollar_string.$cents.$cents_string;
        $res = $this->prepositionFill( $string );
        return $res;

    }


    /**
     * Insert preposition 'And' on string
     * @param $string
     * @return mixed
     */
    private function prepositionFill( $string )
    {
        $units = [ 'trillion' , 'billion' , 'million' , 'thousand' , 'hundred'];
        foreach( $units as $needle ){
            $string = str_replace( $needle , $needle . ' and' , $string );
        }
        //special case
        $string = str_replace('and dollars' , 'dollars' , $string );
        return $string;
    }
}