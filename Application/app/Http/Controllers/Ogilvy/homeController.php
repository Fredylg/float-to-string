<?php

namespace App\Http\Controllers\Ogilvy;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\FloatToString;

/**
 * Handles incoming request from index get/post
 * Class homeController
 * @package App\Http\Controllers\Ogilvy
 */
class homeController extends Controller
{
    /**
     * Instance of helper class
     * @var FloatToString
     */
    protected $helper;

    /**
     * @param FloatToString $helper
     */
    public function __construct(FloatToString $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Display Form
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('page');
    }
    /**
     * Process and render data
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function render(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:200',
            'amount' => 'required|numeric|min:0.01',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }

        try{
            $string_name  =  trim( ucfirst(strtolower( $request->name )));
            $string_value = mb_strtoupper ( strtolower( $this->helper->getString( $request->amount ) ) ) ;
        }catch (\Exemption $e){
            return redirect('/')->withInput();
        }

        return view('cheque', ['name' => $string_name , 'amount' => $string_value ]);
    }

}