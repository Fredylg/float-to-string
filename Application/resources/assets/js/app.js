var application = {
    lastKey : null,
    init: function(){
        this.set();

    },
    set: function(){
        $("#form").submit(function (e) {
            var patt = /^[0-9]+(\.[0-9]{1,2})?$/
            var val =  parseFloat($("#amount").val(),10).toFixed(2);
            $("#amount").val(val);
            if ( patt.test( val ) == true ) {
              return true;
            }
            $("#error-message").html("Please check the amount field.");
            $("#error-box").removeClass('hidden');
            return false;
        });
        $('[data-toggle="tooltip"]').tooltip();

    }
};

$( document ).ready(function() {
    application.init();
});



