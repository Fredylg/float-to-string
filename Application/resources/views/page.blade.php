@include('partials.head')
        @include('partials.head')
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="page-header">
                        <div class="col-sm-12">
                            <h2>Cheque printing system</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p>Please enter name and cheque amount.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 @if (count($errors) <= 0) hidden @endif" id="error-box" >
                         <div class="alert alert-danger alert-dismissible fade in" role="alert">
                           <h4>Error</h4>
                           <p id="error-message">
                               @if (count($errors) > 0)

                                       <ul>
                                           @foreach ($errors->all() as $error)
                                               <li>{{ $error }}</li>
                                           @endforeach
                                       </ul>

                               @endif
                           </p>
                         </div>

                    </div>
                </div>
                <div class="jumbotron">
                     <div class="row">
                        <div class="col-sm-12">
                                {!! Form::open(array('url' => '/' , 'id' => 'form')) !!}
                                  <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" required="true"  maxlength="200" />
                                  </div>
                                  <div class="form-group"><br/>
                                    <label for="amount">Amount</label>
                                    <input type="number" step="0.01" class="form-control" id="amount" name="amount" placeholder="Amount" required="true" data-toggle="tooltip" data-placement="top" title="Use only two decimal points" />
                                   </div>
                                     <button type="submit" class="btn btn-default">Submit</button>
                                {!! Form::close() !!}
                        </div>
                     </div>
                 </div>
            </div>
        </div>
        @include('partials.footer')
    </body>
</html>
